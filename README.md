Развертывание инфраструктуры с помощью Ansible
==============================================

Модуль использует переменную для указания пути к директории инвентаря: *ANSIBLE_INVENTORIES_PATH*. Если эта переменная не будет задана, то путь к инвентарю будет таким: *./ansible/inventories*.  
еще одна переменная: *ANSIBLE_PATH* - путь к директории, где расположены файлы *playbook.yml* и *requirements.yml*. Если эта переменная не будет задана, то директория будет выбрана как: *./ansible*. 